#include <nori/bsdf.h>
#include <nori/emitter.h>
#include <nori/shape.h>

NORI_NAMESPACE_BEGIN

    typedef struct {
        double t;
        Normal3f n;
    } t;

    bool operator<(t const &t1, t const &t2) {
        return t1.t < t2.t;
    }

    bool operator>(t const &t1, t const &t2) {
        return t1.t < t2.t;
    }

    class Box : public Shape {
    public:
        Box(const PropertyList &props) {}

        void activate() {
            if (!m_bsdf) {
                /* If no material was assigned, instantiate a diffuse BRDF */
                m_bsdf = static_cast<BSDF *>(
                        NoriObjectFactory::createInstance("diffuse", PropertyList()));
            }
        }

        bool rayIntersect(Ray3f &ray, Intersection &its,
                          bool shadowRay = false) const {

            t x1, x2, y1, y2, z1, z2;
            x1.t = (-0.5 - ray.o.x()) / ray.d.x();
            x1.n = Normal3f(-1, 0, 0);
            x2.t = (0.5 - ray.o.x()) / ray.d.x();
            x2.n = Normal3f(1, 0, 0);
            y1.t = (-0.5 - ray.o.y()) / ray.d.y();
            y1.n = Normal3f(0, -1, 0);
            y2.t = (0.5 - ray.o.y()) / ray.d.y();
            y2.n = Normal3f(0, 1, 0);
            z1.t = (-0.5 - ray.o.z()) / ray.d.z();
            z1.n = Normal3f(0, 0, -1);
            z2.t = (0.5 - ray.o.z()) / ray.d.z();
            z2.n = Normal3f(0, 0, 1);

            const t& txnear = std::min(x1, x2);
            const t& txfar = std::max(x1, x2);
            const t& tynear = std::min(y1, y2);
            const t& tyfar = std::max(y1, y2);
            const t& tznear = std::min(z1, z2);
            const t& tzfar = std::max(z1, z2);

            t tnear = std::max({txnear, tynear, tznear});
            t tfar = std::min({txfar, tyfar, tzfar});

            if (tfar < tnear) {
                return false;
            }

            if (tnear.t >= ray.mint and tnear.t <= ray.maxt) {
                if (shadowRay) {
                    return true;
                }
                updateRayAndHit(ray, its, tnear.t, tnear.n);
                return true;
            } else if (tfar.t >= ray.mint and tfar.t <= ray.maxt) {
                if (shadowRay) {
                    return true;
                }
                updateRayAndHit(ray, its, tfar.t, tfar.n);
                return true;
            }
            return false;
        }

        /// Register a child object (e.g. a BSDF) with the mesh
        void addChild(NoriObject *obj) {
            switch (obj->getClassType()) {
                case EBSDF:
                    if (m_bsdf)
                        throw NoriException(
                                "Plane: tried to register multiple BSDF instances!");
                    m_bsdf = static_cast<BSDF *>(obj);
                    break;

                case EEmitter: {
                    Emitter *emitter = static_cast<Emitter *>(obj);
                    if (m_emitter)
                        throw NoriException(
                                "Plane: tried to register multiple Emitter instances!");
                    m_emitter = emitter;
                }
                    break;

                default:
                    throw NoriException("Plane::addChild(<%s>) is not supported!",
                                        classTypeName(obj->getClassType()));
            }
        }

        std::string toString() const {
            return tfm::format(
                    "Plane[\n"
                    "emitter = %s\n"
                    "bsdf = %s\n"
                    "]",
                    (m_emitter) ? indent(m_emitter->toString()) : std::string("null"),
                    (m_bsdf) ? indent(m_bsdf->toString()) : std::string("null"));
        }

    private:
        void updateRayAndHit(Ray3f &ray, Intersection &its, float t,
                             const Normal3f &n) const {
            ray.maxt = its.t = t;
            its.p = ray(its.t);
            its.uv = Point2f(its.p.x(), its.p.z());
            its.bsdf = m_bsdf;
            its.emitter = m_emitter;
            its.geoFrame = its.shFrame = Frame(n);
        }

    private:
        BSDF *m_bsdf = nullptr;       ///< BSDF of the surface
        Emitter *m_emitter = nullptr; ///< Associated emitter, if any
    };

    NORI_REGISTER_CLASS(Box, "box");
NORI_NAMESPACE_END
