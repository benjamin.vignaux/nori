#include <nori/bsdf.h>
#include <nori/emitter.h>
#include <nori/shape.h>

NORI_NAMESPACE_BEGIN
    class Sphere : public Shape {
    public:
        Sphere(const PropertyList &props) {}

        void activate() {
            if (!m_bsdf) {
                /* If no material was assigned, instantiate a diffuse BRDF */
                m_bsdf = static_cast<BSDF *>(
                        NoriObjectFactory::createInstance("diffuse", PropertyList()));
            }
        }

        bool rayIntersect(Ray3f &ray, Intersection &its,
                          bool shadowRay = false) const {

            float a = ray.d.squaredNorm();
            float b = 2 * ray.o.dot(ray.d);
            float c = ray.o.squaredNorm() - 1;

            float delta = b * b - 4 * a * c;

            if (delta < 0) {
                return false;
            }

            float t1 = (-b - sqrt(delta)) / (2 * a);
            float t2 = (-b + sqrt(delta)) / (2 * a);

            if (t1 >= ray.mint and t1 <= ray.maxt) {
                if (shadowRay) {
                    return true;
                }
                updateRayAndHit(ray, its, t1);
                return true;
            } else if (t2 >= ray.mint and t2 <= ray.maxt){
                if (shadowRay) {
                    return true;
                }
                updateRayAndHit(ray, its, t2);
                return true;
            }
            return false;
        }

        /// Register a child object (e.g. a BSDF) with the mesh
        void addChild(NoriObject *obj) {
            switch (obj->getClassType()) {
                case EBSDF:
                    if (m_bsdf)
                        throw NoriException(
                                "Plane: tried to register multiple BSDF instances!");
                    m_bsdf = static_cast<BSDF *>(obj);
                    break;

                case EEmitter: {
                    Emitter *emitter = static_cast<Emitter *>(obj);
                    if (m_emitter)
                        throw NoriException(
                                "Plane: tried to register multiple Emitter instances!");
                    m_emitter = emitter;
                } break;

                default:
                    throw NoriException("Plane::addChild(<%s>) is not supported!",
                                        classTypeName(obj->getClassType()));
            }
        }

        std::string toString() const {
            return tfm::format(
                    "Plane[\n"
                    "emitter = %s\n"
                    "bsdf = %s\n"
                    "]",
                    (m_emitter) ? indent(m_emitter->toString()) : std::string("null"),
                    (m_bsdf) ? indent(m_bsdf->toString()) : std::string("null"));
        }

    private:
        void updateRayAndHit(Ray3f &ray, Intersection &its, float t) const {
            ray.maxt = its.t = t;
            its.p = ray(its.t);
            its.uv = Point2f(its.p.x(), its.p.z());
            its.bsdf = m_bsdf;
            its.emitter = m_emitter;
            Normal3f n(its.p);
            its.geoFrame = its.shFrame = Frame(n);
        }

    private:
        BSDF *m_bsdf = nullptr;       ///< BSDF of the surface
        Emitter *m_emitter = nullptr; ///< Associated emitter, if any
    };

NORI_REGISTER_CLASS(Sphere, "sphere");
NORI_NAMESPACE_END
