#include <nori/bsdf.h>
#include <nori/emitter.h>
#include <nori/shape.h>

NORI_NAMESPACE_BEGIN
class Instance : public Shape {
private:
    Shape* _shape;
    Transform _toWorld;
    Transform _toObject;
public:
  Instance(const PropertyList &props) {
      _toWorld = props.getTransform("toWorld");
      _toObject = _toWorld.inverse();
  }

  void activate() {
    if (!m_bsdf) {
      /* If no material was assigned, instantiate a diffuse BRDF */
      m_bsdf = static_cast<BSDF *>(
          NoriObjectFactory::createInstance("diffuse", PropertyList()));
    }
  }

  bool rayIntersect(Ray3f &ray, Intersection &its,
                    bool shadowRay = false) const {

      Ray3f newRay = _toObject * ray;

      if (_shape->rayIntersect(newRay, its, shadowRay)) {

          if (not shadowRay) {
              ray.maxt = newRay.maxt;
              its.p = _toWorld * its.p;
              its.shFrame.n = _toWorld * its.shFrame.n;
              its.shFrame.s = _toWorld * its.shFrame.s;
              its.shFrame.t = _toWorld * its.shFrame.t;
          }
          return true;
      }
      return false;
  }

  /// Register a child object (e.g. a BSDF) with the mesh
  void addChild(NoriObject *obj) {
    switch (obj->getClassType()) {
        case EShape:
            _shape = dynamic_cast<Shape*>(obj);

        case EBSDF:
          if (m_bsdf)
            throw NoriException(
                "Plane: tried to register multiple BSDF instances!");
          m_bsdf = static_cast<BSDF *>(obj);
          break;

        case EEmitter: {
          Emitter *emitter = static_cast<Emitter *>(obj);
          if (m_emitter)
            throw NoriException(
                "Plane: tried to register multiple Emitter instances!");
          m_emitter = emitter;
        } break;



        default:
          throw NoriException("Plane::addChild(<%s>) is not supported!",
                              classTypeName(obj->getClassType()));
        }
  }

  std::string toString() const {
    return tfm::format(
        "Plane[\n"
        "emitter = %s\n"
        "bsdf = %s\n"
        "]",
        (m_emitter) ? indent(m_emitter->toString()) : std::string("null"),
        (m_bsdf) ? indent(m_bsdf->toString()) : std::string("null"));
  }

private:
  void updateRayAndHit(Ray3f &ray, Intersection &its, float t,
                       const Normal3f &n) const {
    ray.maxt = its.t = t;
    its.p = ray(its.t);
    its.uv = Point2f(its.p.x(), its.p.z());
    its.bsdf = m_bsdf;
    its.emitter = m_emitter;
    its.geoFrame = its.shFrame = Frame(n);
  }

private:
  BSDF *m_bsdf = nullptr;       ///< BSDF of the surface
  Emitter *m_emitter = nullptr; ///< Associated emitter, if any
};

NORI_REGISTER_CLASS(Instance, "instance");
NORI_NAMESPACE_END
