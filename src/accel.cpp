/*
    This file is part of Nori, a simple educational ray tracer
    Copyright (c) 2015 by Wenzel Jakob
    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.
    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <nori/accel.h>
#include <nori/timer.h>
#include <tbb/tbb.h>
#include <Eigen/Geometry>
#include <atomic>
#include <octTree.h>

/*
 * =======================================================================
 *   WARNING    WARNING    WARNING    WARNING    WARNING    WARNING
 * =======================================================================
 *   Remember to put on SAFETY GOGGLES before looking at this file. You
 *   are most certainly not expected to read or understand any of it.
 * =======================================================================
 */

NORI_NAMESPACE_BEGIN


    void Accel::addMesh(Mesh *mesh) {
        m_meshes.push_back(mesh);
        m_meshOffset.push_back(m_meshOffset.back() + mesh->getTriangleCount());
        m_bbox.expandBy(mesh->getBoundingBox());
    }

    void Accel::clear() {
        for (auto mesh : m_meshes)
            delete mesh;
        m_meshes.clear();
        m_meshOffset.clear();
        m_meshOffset.push_back(0u);
        m_bbox.reset();
        m_meshes.shrink_to_fit();
        m_meshOffset.shrink_to_fit();
    }

    void Accel::activate() {
        uint32_t size  = getTriangleCount();
        if (size == 0)
            return;
        cout << "Constructing a SAH BVH (" << m_meshes.size()
             << (m_meshes.size() == 1 ? " mesh, " : " meshes, ")
             << size << " triangles) .. ";
        cout.flush();
        Timer timer;

        _tree = new octTree(&m_meshes);

        cout << "done (took " << timer.elapsedString() << ")." << endl;

    }

    bool Accel::rayIntersect(Ray3f &_ray, Intersection &its, bool shadowRay) const {
        return _tree->rayIntersect(_ray, its, shadowRay);
    }

    std::string Accel::toString() const {
        std::string meshes;
        for (size_t i=0; i<m_meshes.size(); ++i) {
            meshes += std::string("  ") + indent(m_meshes[i]->toString(), 2);
            if (i + 1 < m_meshes.size())
                meshes += ",";
            meshes += "\n";
        }
        return tfm::format(
                "BBOX accel[\n"
                "  meshes = {\n"
                "  %s  }\n"
                "]",
                indent(meshes, 2)
        );
    }


NORI_NAMESPACE_END