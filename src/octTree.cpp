//
// Created by benjamin on 30/09/2020.
//

#include "octTree.h"

bool octTree::rayIntersect(Ray3f &_ray, Intersection &its, bool shadowRay) const {
    /* Use an adaptive ray epsilon */
    Ray3f ray(_ray);
    if (ray.mint == Epsilon)
        ray.mint = std::max(ray.mint, ray.mint * ray.o.array().abs().maxCoeff());

    if (ray.maxt < ray.mint)
        return false;

    bool foundIntersection = false;
    const Mesh *hitmesh   = nullptr;
    uint32_t f = 0;

    std::vector<Triangle*> triangles = getTriangles();
    BoundingBox3f box = createFirstBox();

    Node* node = build(box, triangles);

    if (visitTree(node, ray, f, (Mesh &) hitmesh, its, foundIntersection, shadowRay)) {
        return true;
    }

    if (foundIntersection) {
        /* Find the barycentric coordinates */
        Vector3f bary;
        bary << 1-its.uv.sum(), its.uv;

        /* References to all relevant mesh buffers */
        const MatrixXf &V  = hitmesh->getVertexPositions();
        const MatrixXf &N  = hitmesh->getVertexNormals();
        const MatrixXf &UV = hitmesh->getVertexTexCoords();
        const MatrixXu &F  = hitmesh->getIndices();

        /* Vertex indices of the triangle */
        uint32_t idx0 = F(0, f), idx1 = F(1, f), idx2 = F(2, f);

        Point3f p0 = V.col(idx0), p1 = V.col(idx1), p2 = V.col(idx2);

        /* Compute the intersection positon accurately
           using barycentric coordinates */
        its.p = bary.x() * p0 + bary.y() * p1 + bary.z() * p2;

        /* Compute proper texture coordinates if provided by the mesh */
        if (UV.size() > 0)
            its.uv = bary.x() * UV.col(idx0) +
                     bary.y() * UV.col(idx1) +
                     bary.z() * UV.col(idx2);

        /* Compute the geometry frame */
        its.geoFrame = Frame((p1-p0).cross(p2-p0).normalized());

        if (N.size() > 0) {
            /* Compute the shading frame. Note that for simplicity,
               the current implementation doesn't attempt to provide
               tangents that are continuous across the surface. That
               means that this code will need to be modified to be able
               use anisotropic BRDFs, which need tangent continuity */

            its.shFrame = Frame(
                    (bary.x() * N.col(idx0) +
                     bary.y() * N.col(idx1) +
                     bary.z() * N.col(idx2)).normalized());
        } else {
            its.shFrame = its.geoFrame;
        }

        if(hitmesh->isEmitter()) {
            its.emitter=hitmesh->getEmitter();
        }
    }

    return foundIntersection;
}

octTree::octTree(std::vector<Mesh *> *meshes) {
    _meshes = meshes;
}

std::vector<BoundingBox3f> octTree::createSubBoxes(const BoundingBox3f& box) {
    std::vector<BoundingBox3f> boxes;
    float Xwidth = (box.max.x() - box.min.x()) / 2;
    float Ywidth = (box.max.y() - box.min.y()) / 2;
    float Zwidth = (box.max.z() - box.min.z()) / 2;

    Point3f point1 = box.min;
    Point3f point2 = box.getCenter();

    boxes.emplace_back(BoundingBox3f(point1, point2));
    boxes.emplace_back(BoundingBox3f(point1 + Point3f(0, Ywidth, 0), point2 + Point3f(0, Ywidth, 0)));
    boxes.emplace_back(BoundingBox3f(point1 + Point3f(0, 0, Zwidth), point2 + Point3f(0, 0, Zwidth)));
    boxes.emplace_back(BoundingBox3f(point1 + Point3f(0, Ywidth, Zwidth), point2 + Point3f(0, Ywidth, Zwidth)));
    boxes.emplace_back(BoundingBox3f(point1 = Point3f(Xwidth, 0, 0), point2 + Point3f(Xwidth, 0, 0)));
    boxes.emplace_back(BoundingBox3f(point1 + Point3f(Xwidth, Ywidth, 0), point2 + Point3f(Xwidth, Ywidth, 0)));
    boxes.emplace_back(BoundingBox3f(point1 + Point3f(Xwidth, 0, Zwidth), point2 + Point3f(Xwidth, 0, Zwidth)));
    boxes.emplace_back(BoundingBox3f(point1 + Point3f(Xwidth, Ywidth, Zwidth), point2 + Point3f(Xwidth, Ywidth, Zwidth)));

    return boxes;
}

Node * octTree::build(const BoundingBox3f& box, const std::vector<Triangle*>& triangles) const {
    if (triangles.empty()) {
        return nullptr;
    }

    if (triangles.size() <= 10) {
        Node* node = new Node();
        for (Triangle* triangle : triangles) {
            node->triangles.push_back(triangle);
        }
    }

    std::vector<std::vector<Triangle*>> list;
    std::vector<BoundingBox3f> subBoxes = createSubBoxes(box);

    for (Triangle *triangle : triangles) {
        for (int i = 0; i < 8; i++) {
            BoundingBox3f triangleBox = triangle->mesh->getBoundingBox(triangle->num);
            if (triangleBox.overlaps(subBoxes[i])) {
                list[i].push_back(triangle);
            }
        }
    }

    Node* node = new Node();
    for (int i = 0; i < 8; i++) {
        node->child.push_back(build(subBoxes[i], list[i]));
    }
}

BoundingBox3f octTree::createFirstBox() const {
    BoundingBox3f box = _meshes->at(0)->getBoundingBox();

    for (int i = 1; i < _meshes->size(); ++i) {
        box = BoundingBox3f::merge(box, _meshes->at(i)->getBoundingBox());
    }

    return box;
}

std::vector<Triangle*> octTree::getTriangles() const {
    std::vector<Triangle*> triangles;

    for (Mesh*  mesh : *_meshes) {
        for (int i = 0; i < mesh->getTriangleCount(); i++) {
            triangles.emplace_back(new Triangle(mesh, i));
        }
    }

    return triangles;
}

bool octTree::visitTree(Node *node, Ray3f &ray, uint32_t &f, Mesh &hitmesh, Intersection &its, bool &foundIntersection, bool shadowRay) const {
    for(Node* child : node->child) {
        if (child->triangles.empty()) {
            return visitTree(child, ray, f, hitmesh, its, foundIntersection, shadowRay);
        } else {
            for (Triangle* triangle : child->triangles) {
                float u, v, t;
                if (triangle->mesh->rayIntersect(triangle->num, ray, u, v, t)) {
                    /* An intersection was found! Can terminate
                       immediately if this is a shadow ray query */
                    if (shadowRay)
                        return true;
                    ray.maxt = its.t = t;
                    its.uv = Point2f(u, v);
                    hitmesh = *triangle->mesh;
                    f = triangle->num;
                    foundIntersection = true;
                }
            }
            return false;
        }
    }
}

Triangle::Triangle(Mesh *mesh, int num) {
    this->mesh = mesh;
    this->num = num;
}
