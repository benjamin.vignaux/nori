//
// Created by benjamin on 30/09/2020.
//

#ifndef NORI_OCTTREE_H
#define NORI_OCTTREE_H

#include <nori/shape.h>
#include "nori/mesh.h"
#include "nori/common.h"
#include "nori/bbox.h"

using namespace nori;

class Triangle {
public:
    Triangle(Mesh* mesh, int num);
    Mesh* mesh;
    int num;
};

class Node {
public:
    std::vector<Node*> child;
    std::vector<Triangle*> triangles;
};

class octTree {
private:
    std::vector<Mesh *>* _meshes;

public:
    explicit octTree(std::vector<Mesh *> *meshes);
    bool rayIntersect(Ray3f &_ray, Intersection &its, bool shadowRay) const;
    BoundingBox3f createFirstBox() const;
    static std::vector<BoundingBox3f> createSubBoxes(const BoundingBox3f& box);
    Node * build(const BoundingBox3f& box, const std::vector<Triangle*>& triangles) const;
    std::vector<Triangle*> getTriangles() const;
    bool visitTree(Node *node, Ray3f &ray, uint32_t &f, Mesh &hitmesh, Intersection &its, bool &foundIntersection, bool shadowRay) const;
};


#endif //NORI_OCTTREE_H
